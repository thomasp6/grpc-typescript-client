import { ProjectClientImpl, ProjectResponse } from "./generated/src/proto/project"
import { rpc } from "./rpc"

const projectService = new ProjectClientImpl(rpc)

const project: ProjectResponse = await projectService.createProject({
    name: "foo",
    environment: "sandbox",
    flags: ['live_stream:force_records'],
})

console.log(project)