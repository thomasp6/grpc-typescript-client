gRPC client implementation in Typescript
---- 

First install dependencies with `npm install`:
* `@grpc/grpc-js`
* `@grpc/proto-loader`
* `ts-proto`

Create your proto files in the `src/proto` dir (only `project.proto` for now)

Compile them with `protoc` thanks to the following command: 
```sh
npm run generate-proto
```
It will output them in the `./src/generated/src/proto` folder.

Then, update the server address and credentials in `./rpc.ts`:
```ts
const conn = new Client(
    "localhost:8765",
    ChannelCredentials.createInsecure(),
);
```

You are now able to manipulate the project service (like described in `./index.ts`):

```ts
import { ProjectClientImpl } from "./generated/src/proto/project"
import { rpc } from "./rpc"

const projectService = new ProjectClientImpl(rpc)

const project: ProjectResponse = await projectService.createProject({
    name: "foo",
    environment: "sandbox",
    flags: ['live_stream:force_records'],
})
```